FROM php:7.3-apache
FROM phpmyadmin

COPY ./apps /var/www/html
RUN docker-php-ext-install mysqli

EXPOSE 80